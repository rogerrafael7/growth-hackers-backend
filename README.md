# Aplicação de Teste Backend:: Growth Hackers

### Pré-requisitos
Na raiz do projeto primeiramente deve-se executar:
> docker-compose up -d
> 
> npm install

Com isso será criado o banco de dados, e a instalação das dependências do projeto.


### Para rodar o projeto em ambiente de `produção`:
> npm run build
> 
> npm run migrations:run-prod
> 
> npm run start:prod

### Para rodar o projeto em ambiente de `desenvolvimento`:
> npm run migrations:run-development
>
> npm run start:dev

### Para rodar os testes:
> npm run migrations:run-test
> 
> npm run test:e2e


#### Observações:
O arquivo de env está versionado apenas para demonstração.
