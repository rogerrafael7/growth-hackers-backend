import * as dotenv from 'dotenv';
dotenv.config();

export const enviroment = {
  PORT: process.env.PORT,

  DB_HOST: process.env.POSTGRES_HOST,
  DB_PORT: process.env.POSTGRES_PORT,
  DB_USERNAME: process.env.POSTGRES_USER,
  DB_PASSWORD: process.env.POSTGRES_PASSWORD,
  DB_NAME: process.env.POSTGRES_DB,
  DB_SCHEMA: process.env.DB_SCHEMA,
  NODE_ENV: process.env.NODE_ENV,
};
